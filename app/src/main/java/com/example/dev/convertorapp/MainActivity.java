package com.example.dev.convertorapp;

import android.icu.math.BigDecimal;
import android.icu.text.DateFormat;
import android.icu.text.DecimalFormat;
import android.icu.text.NumberFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private Spinner spinner;
    private EditText editText1;
    private EditText editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText1 = (EditText) findViewById(R.id.txtConversion1);
        editText2 = (EditText) findViewById(R.id.txtConversion2);
        spinner =  (Spinner) findViewById(R.id.conversionType);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();

                editText1.setText("");
                editText2.setText("");

                if (selectedItem.equals(Conversions.toTbsp)){
                    editText1.setText("1");
                    double val = Conversions.toTbsp(1);
                    editText2.setText(String.valueOf(val));

                }
                else if (selectedItem.equals(Conversions.toMilli)){
                    editText1.setText("1");
                    double val = Conversions.toMilli(1);
                    editText2.setText(String.valueOf(val));
                }
                else if (selectedItem.equals(Conversions.toQuart)){
                    editText1.setText("1");
                    double val = Conversions.toQuart(1);
                    editText2.setText(String.valueOf(val));
                }
                else if (selectedItem.equals(Conversions.toOunce)){
                    editText1.setText("1");
                    double val = Conversions.toOunce(1);
                    editText2.setText(String.valueOf(val));
                }
                else if (selectedItem.equals(Conversions.toKg)){
                    editText1.setText("1");
                    double val = Conversions.toKg(1);
                    editText2.setText(String.valueOf(val));
                }
                else if (selectedItem.equals(Conversions.toCm)){
                    editText1.setText("1");
                    double val = Conversions.toCenti(1);
                    editText2.setText(String.valueOf(val));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editText1.addTextChangedListener(watcher1);
        editText2.addTextChangedListener(watcher2);

    }

    /*Text watcher for 1st text box*/

    TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            boolean userchange = Math.abs(count - before) == 1;
            if (userchange){

                if (spinner.getSelectedItem().equals(Conversions.toTbsp) && editText1.isFocused() && count != 0) {
                    double value = Double.parseDouble(editText1.getText().toString());
                    double res = Conversions.toTbsp(value);
                    editText2.setText(String.valueOf(String.format("%.3f",res)));
                }

                if (spinner.getSelectedItem().equals(Conversions.toMilli) && count != 0 && editText1.isFocused()){
                    double value = Double.parseDouble(editText1.getText().toString());
                    double res = Conversions.toMilli(value);
                    editText2.setText(String.valueOf(res));
                }

                if (spinner.getSelectedItem().equals(Conversions.toQuart) && count != 0 && editText1.isFocused()){
                    double value = Double.parseDouble(editText1.getText().toString());
                    double res = Conversions.toQuart(value);
                    editText2.setText(String.valueOf(String.format("%.3f", res)));
                }

                if (spinner.getSelectedItem().equals(Conversions.toOunce) && count != 0 && editText1.isFocused()){
                    double value = Double.parseDouble(editText1.getText().toString());
                    double res = Conversions.toOunce(value);
                    editText2.setText(String.valueOf(res));
                }

                if (spinner.getSelectedItem().equals(Conversions.toKg) && count != 0 && editText1.isFocused()){
                    double value = Double.parseDouble(editText1.getText().toString());
                    double res = Conversions.toKg(value);
                    editText2.setText(String.valueOf(res));
                }

                if (spinner.getSelectedItem().equals(Conversions.toCm) && count != 0 && editText1.isFocused()){
                    double value = Double.parseDouble(editText1.getText().toString());
                    double res = Conversions.toCenti(value);
                    editText2.setText(String.valueOf(String.format("%.3f", res)));
                }
                editText1.setSelection(editText1.getText().length());


                if (count == 0)
                    editText2.setText("");
            }

        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    // text watcher for 2nd text box

    TextWatcher watcher2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            boolean userchange = Math.abs(count - before) == 1;

            if (userchange){

                if (spinner.getSelectedItem().equals(Conversions.toTbsp) && editText2.isFocused() && count != 0) {
                    double value = Double.parseDouble(editText2.getText().toString());
                    double res = Conversions.toTsp(value);
                    editText1.setText(String.valueOf(Math.round(res)));
                }

                if (spinner.getSelectedItem().equals(Conversions.toMilli) && count != 0 && editText2.isFocused()){
                    double value = Double.parseDouble(editText2.getText().toString());
                    double res = Conversions.toLiter(value);
                    editText1.setText(String.valueOf(res));
                }

                if (spinner.getSelectedItem().equals(Conversions.toQuart) && count != 0 && editText2.isFocused()){
                    double value = Double.parseDouble(editText2.getText().toString());
                    double res = Conversions.toPint(value);
                    editText1.setText(String.valueOf(String.format("%.3f", res)));
                }

                if (spinner.getSelectedItem().equals(Conversions.toOunce) && count != 0 && editText2.isFocused()){
                    double value = Double.parseDouble(editText2.getText().toString());
                    double res = Conversions.toPound(value);
                    editText1.setText(String.valueOf(res));
                }

                if (spinner.getSelectedItem().equals(Conversions.toKg) && count != 0 && editText2.isFocused()){
                    double value = Double.parseDouble(editText2.getText().toString());
                    double res = Conversions.toGram(value);
                    editText1.setText(String.valueOf(Math.round(res)));
                }

                if (spinner.getSelectedItem().equals(Conversions.toCm) && count != 0 && editText2.isFocused()){
                    double value = Double.parseDouble(editText2.getText().toString());
                    double res = Conversions.toInch(value);
                    editText1.setText(String.valueOf(String.format("%.3f", res)));
                }

                editText2.setSelection(editText2.getText().length());

                if (count == 0)
                    editText1.setText("");
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


}
